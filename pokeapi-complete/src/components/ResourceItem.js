import React, { useState, useEffect } from "react";
import { getResource } from "../helpers/apiCalls";

export default function ResourceItem({ resource, sprite_key }) {
    const [resourceDetails, setResourceDetails] = useState({})

    const init = async () => {
        const res = await getResource(resource.url)
        setResourceDetails(res.data)
    }

    useEffect(() => {
        init()
    }, [])

    return (

        <div class="group relative">
            <div class="min-h-80 aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-80">
                {
                    Object.keys(resourceDetails).length > 0
                        ? <img src={resourceDetails.sprites[sprite_key]} alt="Front of men&#039;s Basic Tee in black." class="h-full w-full object-cover object-center lg:h-full lg:w-full" />
                        : <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pok%C3%A9mon_logo.svg/1200px-International_Pok%C3%A9mon_logo.svg.png' alt="Front of men&#039;s Basic Tee in black." class="h-full w-full object-cover object-center lg:h-full lg:w-full" />
                }

            </div>
            <div class="mt-4 flex justify-between">
                <div>
                    <h3 class="text-sm text-gray-700">
                        <a href="#">
                            <span aria-hidden="true" class="absolute inset-0"></span>
                            Nombre <b>{resource.name.toUpperCase()}</b>
                        </a>
                    </h3>
                    <h3 class="text-sm text-gray-700">
                    </h3>
                </div>
            </div>
        </div>
    )
}