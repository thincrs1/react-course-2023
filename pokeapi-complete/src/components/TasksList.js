import TaskItem from "./TaskItem";

export default function TasksLisk({ tasks, onDeleteTask }) {

    return (
        <>
            {
                tasks.map((task, index) => {
                    return (
                        <TaskItem
                            task={task}
                            index={index}
                            onDeleteTask={onDeleteTask}
                        />
                    )
                })
            }

        </>
    )

}