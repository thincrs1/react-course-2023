

export default function CustomButton({ children, onClick }) {
    return (
        <button className='p-2 bg-indigo-500 rounded text-white mr-3' onClick={() => onClick()}>
            {children}
        </button>
    )
}