import './index.css'
import Filters from './pages/Filters'
import { useState, useEffect } from 'react'
import { getDataFromResource, getNewData, getResource } from './helpers/apiCalls'
import PokemonList from './components/PokemonList'

export default function App() {

    const [pokemons, setPokemons] = useState([])
    const [nextPage, setNextPage] = useState()
    const [prevPage, setPrevPage] = useState()

    const [filters, setFilters] = useState({
        habilities: {},
        types: {}
    })

    const setPokemonsData = data => {
        setNextPage(data.next)
        setPrevPage(data.previous)
        setPokemons(data.results)
    }

    /*
    const getFilterData = async url => {
        const res = await getResource(url)
        const pokemons = res.data.pokemon.map(entity => entity.pokemon)
        onPokemonListUpdated(pokemons)
    }
    */

    const init = async () => {
        let res = await getDataFromResource('pokemon')
        setPokemonsData(res.data)

        const habilities = await getDataFromResource('ability')
        setFilters((filters) => ({
            ...filters,
            habilities: {
                name: 'Habilidades',
                data: habilities.data.results,
                selectedFilters: []
            }
        }))

        const types = await getDataFromResource('type')
        setFilters((filters) => ({
            ...filters,
            types: {
                name: 'Tipo de pokemon',
                selectedFilters: [],
                data: types.data.results
            }
        }))
    }

    const getDataFromFilterObject = async filter => {
        let pokemons = await getResource(filter.url)
        return pokemons.data.pokemon.map(entity => entity.pokemon)
    }

    const updateFiltersInStateWithData = async (allFilters, newFilters, key) => {
        let allResponses = []
        Object.values(allFilters).forEach(entity => (entity.forEach(fil => allResponses.push(getDataFromFilterObject(fil)))))
        allResponses = await Promise.all(allResponses)

        setFilters({
            ...filters,
            [key]: {
                ...filters[key],
                selectedFilters: newFilters,
            }
        })

        let allPokemons = []
        allResponses.forEach(entity => entity.forEach(pokemon => allPokemons.push(pokemon)))

        setPokemons(allPokemons)
    }

    const deleteFilter = async (filter, key) => {
        // Tomar los filtros del estado y los agrega a una variable local
        let newFilters = [...filters[key].selectedFilters]
        // Genera la estructura de filtros original en una variable local
        let allFilters = {
            habilities: [...filters['habilities']['selectedFilters']],
            types: [...filters['types']['selectedFilters']],
        }

        // Filtrar para que el filtro que se quiere eliminar salga del arreglo
        newFilters = newFilters.filter(objectFilter => objectFilter.name !== filter.name)
        // Esto es lo que agrega a la estructura el filtro nuevo
        allFilters[key] = newFilters

        // Función encargada de obtener la información nueva y actualizar el estado
        updateFiltersInStateWithData(allFilters, newFilters, key)
    }

    const updateFilters = async (filter, key) => {
        // Tomar los filtros del estado y los agrega a una variable local
        const newFilters = [...filters[key].selectedFilters]
        // Agrega el nuevo filtro a nuestra variable local
        newFilters.push(filter)

        // Genera la estructura de filtros original en una variable local
        let allFilters = {
            habilities: [...filters['habilities']['selectedFilters']],
            types: [...filters['types']['selectedFilters']],
        }
        // Esto es lo que agrega a la estructura el filtro nuevo
        allFilters[key] = newFilters

        // Función encargada de obtener la información nueva y actualizar el estado
        updateFiltersInStateWithData(allFilters, newFilters, key)
    }


    const getNewPage = async url => {
        const res = await getNewData(url)
        setPokemonsData(res.data)
    }

    useEffect(() => {
        init()
    }, [])

    return (
        <>
            <div className="mx-auto max-w-7xl p-4 sm:px-6 lg:px-8" >

                <Filters
                    filters={filters}
                    onNewFilterSelected={({ filter, key }) => updateFilters(filter, key)}
                    onFilterDelete={({ filter, key }) => deleteFilter(filter, key)}
                >

                    <div className="bg-white">

                        {prevPage && <button onClick={() => getNewPage(prevPage)} className='p-2 bg-indigo-500 rounded text-white mr-3'>Página anterior</button>}
                        {nextPage && <button onClick={() => getNewPage(nextPage)} className='p-2 bg-indigo-500 rounded text-white mr-3'>Página siguiente</button>}

                        <div class="mt-6 grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
                            {pokemons.length > 0 && <PokemonList pokemons={pokemons} />}
                        </div>
                    </div>

                </Filters>

            </div >
        </>
    )

}