import { useState } from "react"
import CustomButton from "../ui-kit/CustomButton"


export default function FilterList({ filterList, onNewFilterSelected, onFilterDelete }) {

    const [showFilters, setShowFilters] = useState(true)
    const [showAll, setShowAll] = useState(false)

    return (
        <>
            <div class="border-b border-gray-200 py-6">
                <h3 class="-my-3 flow-root">
                    <button type="button" class="flex w-full items-center justify-between bg-white py-3 text-sm text-gray-400 hover:text-gray-500" aria-controls="filter-section-0" aria-expanded="false">
                        <span class="font-medium text-gray-900">{filterList.name}</span>
                        <span class="ml-6 flex items-center">
                            {
                                !showFilters
                                    ?
                                    <svg onClick={() => setShowFilters(true)} class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path d="M10.75 4.75a.75.75 0 00-1.5 0v4.5h-4.5a.75.75 0 000 1.5h4.5v4.5a.75.75 0 001.5 0v-4.5h4.5a.75.75 0 000-1.5h-4.5v-4.5z" />
                                    </svg>
                                    :
                                    <svg onClick={() => setShowFilters(false)} class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M4 10a.75.75 0 01.75-.75h10.5a.75.75 0 010 1.5H4.75A.75.75 0 014 10z" clip-rule="evenodd" />
                                    </svg>
                            }
                        </span>
                    </button>
                </h3>
                <div class="pt-6" id="filter-section-0">
                    {
                        <div class={showFilters ? 'space-y-4' : 'hidden'} >
                            {
                                filterList.data
                                    .filter(item => {
                                        if (filterList.selectedFilters.filter(selectedFilter => selectedFilter.name === item.name).length > 0) {
                                            return false
                                        } else {
                                            return true
                                        }
                                    })
                                    .slice(0, showAll ? filterList.data.length : 5)
                                    .map(item => {
                                        return (
                                            <div class="flex items-center " onClick={() => onNewFilterSelected({ ...item })}>
                                                <label for="filter-color-0" class="ml-3 text-sm text-gray-600 hover:cursor-pointer hover:text-indigo-500">
                                                    {
                                                        item.name.toUpperCase()
                                                    }
                                                </label>
                                            </div>
                                        )
                                    })}
                            {
                                showAll
                                    ? <button onClick={() => setShowAll(false)}>Ver menos</button>
                                    : <button onClick={() => setShowAll(true)}>Ver todos</button>
                            }

                            {
                                filterList.selectedFilters.map(filter => {
                                    return <div className="flex">
                                        <button
                                            className="p-2 bg-red-500 hover:bg-red-400 rounded-lg text-white"
                                            onClick={() => onFilterDelete({ ...filter })}
                                        >
                                            {filter.name}
                                            <span className="text-bold ml-4">x</span>
                                        </button>
                                    </div>
                                })
                            }
                        </div>
                    }

                </div>
            </div>
        </>
    )
}