import axios from 'axios'

const BASE_URL = 'https://pokeapi.co/api/v2/'

export const getNewData = async URL => {
    try {
        return await getResource(URL)
    } catch (err) {
        return err
    }
}

export const getDataFromResource = async resource => {
    try {
        return await getResource(BASE_URL + resource)
    } catch (err) {
        return err
    }
}

export const getResource = (url) => {
    return new Promise((resolve, reject) => {
        axios.get(url)
            .then(res => resolve(res))
            .catch(err => reject(err))
    })
}