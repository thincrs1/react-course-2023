import App from "../App"
import Catalog from "./Catalog"

export default function Home() {

    const filters = [{
        resource: 'type',
        key: 'types',
        title: 'Tipos de pokemon'
    }, {
        resource: 'ability',
        key: 'habilities',
        title: 'Habilidades de Pokemon'
    }]


    return (
        <>
            <Catalog
                customFilters={filters}
                resource={'pokemon'}
                sprite_key={'front_default'}
            />
        </>
    )
}