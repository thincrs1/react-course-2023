import App from "../App";
import Catalog from "./Catalog";


export default function Items() {

    const filters = [{
        resource: 'item-category',
        key: 'categories',
        title: 'Categorias de los items'
    }, {
        resource: 'item-attribute',
        key: 'attributes',
        title: 'Atributos de los items'
    }]

    return (
        <>
            <Catalog
                customFilters={filters}
                resource={'item'}
                sprite_key={'default'}
            />
        </>
    )
}