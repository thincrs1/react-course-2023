import {Link, useLocation} from "react-router-dom"

export default function Tab({ label, to }) {
    const location = useLocation()
    const isActive = location.pathname === to

    return (
        <>
            {
                isActive
                    ? <Link to={to} class="inline-flex items-center border-b-2 border-indigo-500 px-1 pt-1 text-sm font-medium text-gray-900">{label}</Link>
                    : <Link to={to} class="inline-flex items-center border-b-2 border-transparent px-1 pt-1 text-sm font-medium text-gray-500 hover:border-gray-300 hover:text-gray-700">{label}</Link>
            }
        </>
    )
}