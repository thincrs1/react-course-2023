const sumOfArrays = listOfNumbers => {

    let sum = 0

    for (let i = 0; i < listOfNumbers.length; i++) {
        sum += parseInt(listOfNumbers[i])
    }
    return sum
}
const arrayToProcess = [1,2,3,4]

const avg = arrayToProcess.reduce((accumulator, val) => accumulator + val) / arrayToProcess.length

/*
const newResult = [1,2,3,4].reduce((accumulator, val, index, arr) => {
    return accumulator + val
})
*/


console.log(avg)