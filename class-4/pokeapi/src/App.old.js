import logo from './logo.svg'
import './App.css'
import MyComponent2 from './components/MyComponent2'
import MyComponentClass from './components/MyComponentClass'


function MyComponent() {
  return (
    <>
      <h1>Hi world my component</h1>
    </>
  )
}

function App() {
  return (
    <div className="App">

      <header className="App-header">

        {/* Componente en el mismo archivo */}
        <MyComponent />

        {/* Componente que se importa de otro archivo */}
        <MyComponent2 />

        {/* Componente de tipo clase */}
        <MyComponentClass />

        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
