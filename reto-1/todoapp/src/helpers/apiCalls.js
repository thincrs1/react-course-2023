import axios from 'axios'

const BASE_URL = 'https://pokeapi.co/api/v2/pokemon'


export const getAllPokemons = async () => {
    try {
        return await getResource(BASE_URL)
    } catch (err) {
        return err
    }
}

export const getResource = (url) => {
    return new Promise((resolve, reject) => {
        axios.get(url)
            .then(res => resolve(res))
            .catch(err => reject(err))
    })
}