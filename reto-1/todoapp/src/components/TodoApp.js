import { useState } from "react";
import { LOGO_URL } from "../constants";
import NewList from "../forms/NewList";
import TextInput from "../ui-kit/TextInput";
import TasksLisk from "./TasksList";


export default function TodoApp() {

    const [tasks, setTasks] = useState([])

    const addTask = task => {
        setTasks([...tasks, task])
    }

    const deleteTask = index => {
        const newTasks = [...tasks]

        newTasks.splice(index, 1)

        setTasks(newTasks)
    }

    return (
        <>
            <div class="flex min-h-full flex-col justify-center py-12 sm:px-6 lg:px-8">
                <div class="sm:mx-auto sm:w-full sm:max-w-md">
                    <img class="mx-auto h-10 w-auto" src={LOGO_URL} alt="Your Company" />
                    <h2 class="mt-6 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
                        Aplicación de tareas
                    </h2>
                </div>

                <div class="mt-10 sm:mx-auto sm:w-full sm:max-w-[480px]">
                    <div class="bg-white px-6 py-12 shadow sm:rounded-lg sm:px-12">

                        <NewList
                            onNewTaskAdded={addTask}
                        />

                        <TasksLisk
                            tasks={tasks}
                            onDeleteTask={deleteTask}
                        />

                    </div>

                </div>
            </div>
        </>
    )
}