import PokemonItem from "./PokemonItem"

export default function PokemonList({ pokemons }) {
    return (
        pokemons.map(pokemon => {
            return <PokemonItem pokemon={pokemon} />
        })
    )
}