import { LOGO_URL } from "../constants";
import CheckBox from "../ui-kit/CheckBox";
import TextInput from "../ui-kit/TextInput";
import { useState } from "react"


export default function SignUpForm() {

    const [name, setName] = useState()
    const [phone, setPhone] = useState()
    const [email, setEmail] = useState()
    const [password, setPassword] = useState()
    const [passwordConfirm, setPasswordConfirm] = useState()
    const [users, setUsers] = useState([])

    const registerUser = (e) => {
        e.preventDefault()

        const newUser = { name, phone, email, password }

        setUsers([...users, newUser])
    }

    const test = () => {
        // Clona el arreglo
        let aux = [...users]

        // Elimina un elemento del arreglo
        aux.splice(1, 0)

        console.log(aux)
        // Actualiza el estado
        setUsers(aux)
    }

    return (
        <>
            <div class="flex min-h-full flex-col justify-center py-12 sm:px-6 lg:px-8">
                <div class="sm:mx-auto sm:w-full sm:max-w-md">
                    <img class="mx-auto h-10 w-auto" src={LOGO_URL} alt="Your Company" />
                    <h2 class="mt-6 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
                        Regístrate con nosotros
                    </h2>
                </div>

                <div class="mt-10 sm:mx-auto sm:w-full sm:max-w-[480px]">
                    <div class="bg-white px-6 py-12 shadow sm:rounded-lg sm:px-12">
                        <form
                            class="space-y-6"
                            method="POST"
                            onSubmit={registerUser}
                        >
                            <div>
                                <TextInput
                                    label={'Nombre'}
                                    id={'name'}
                                    type={'text'}
                                    change={val => setName(val)}
                                />
                            </div>
                            <div>
                                <TextInput
                                    label={'Teléfono'}
                                    id={'phone'}
                                    type={'number'}
                                    change={val => setPhone(val)}
                                />
                            </div>
                            <div>
                                <TextInput
                                    label={'Correo electrónico'}
                                    id={'email'}
                                    type={'email'}
                                    change={val => setEmail(val)}
                                />
                            </div>

                            <div>
                                <TextInput
                                    label={'Contraseña'}
                                    id={'password'}
                                    type={'password'}
                                    change={val => setPassword(val)}
                                />
                            </div>
                            <div>
                                <TextInput
                                    label={'Confirmar contraseña'}
                                    id={'password_confirm'}
                                    type={'password_confirm'}
                                    change={val => setPasswordConfirm(val)}
                                />
                            </div>

                            <div class="flex items-center justify-between">
                                <div class="flex items-center">
                                    <CheckBox
                                        id={'remember_me'}
                                        label={'Mantener sesión iniciada'}
                                    />
                                </div>

                                <div class="text-sm leading-6">
                                    <a href="#" class="font-semibold text-indigo-600 hover:text-indigo-500">Forgot password?</a>
                                </div>
                            </div>

                            <div>
                                <button type="submit" class="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">
                                    Regístrate
                                </button>
                            </div>
                        </form>

                        <div>
                            <div class="relative mt-10">
                                <div class="absolute inset-0 flex items-center" aria-hidden="true">
                                    <div class="w-full border-t border-gray-200"></div>
                                </div>
                                <div class="relative flex justify-center text-sm font-medium leading-6">
                                    <span class="bg-white px-6 text-gray-900">Or continue with</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p class="mt-10 text-center text-sm text-gray-500">
                        Not a member?
                        <a href="#" class="font-semibold leading-6 text-indigo-600 hover:text-indigo-500">Start a 14 day free trial</a>
                    </p>
                </div>
            </div>
            <div>
                {
                    users.map((user, index) => {
                        return <>
                            <div key={index}>
                                <ul class="mt-3 grid grid-cols-1 gap-5 sm:grid-cols-2 sm:gap-6 lg:grid-cols-4">
                                    <li class="col-span-1 flex rounded-md shadow-sm">
                                        <div class="flex w-16 flex-shrink-0 items-center justify-center bg-pink-600 rounded-l-md text-sm font-medium text-white">
                                            {user.phone}
                                        </div>
                                        <div class="flex flex-1 items-center justify-between truncate rounded-r-md border-b border-r border-t border-gray-200 bg-white">
                                            <div class="flex-1 truncate px-4 py-2 text-sm">
                                                <a href="#" class="font-medium text-gray-900 hover:text-gray-600">
                                                    {user.name}
                                                </a>
                                                <p class="text-gray-500">{user.email}</p>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </>
                    })
                }
            </div>
        </>
    )
}