import { useState } from 'react'

export default function Memory() {

    const [index, setIndex] = useState(0)

    return (
        <>
            <p>{index}</p>
            <button onClick={() => setIndex(index + 1)}>
                Increment
            </button>
        </>
    )


}