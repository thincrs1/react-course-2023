import './index.css'
import Home from './pages/Home'

export default function App() {

    return (
        <>
            <div class="mx-auto max-w-7xl sm:px-6 lg:px-8" >

                <Home />

            </div >
        </>
    )

}