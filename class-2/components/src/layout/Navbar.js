export default function NavBar({ message, age, callback }) {

    return (
        <>
            <h1>{message}</h1>
            <h2>{age}</h2>
            <button
                className="m-2 bg-gray-500 p-4 rounded-lg text-white"
                onClick={() => {
                    callback(age * 2)
                }}
            >
                Double Age
            </button>
        </>
    )

}