import React from 'react'

export default class Footer extends React.Component {

    render() {
        return (
            <>
                <footer className='bg-black p-40 text-white'>
                    <h1 className='text-2xl'>
                        Este es el footer mas bonito del mundo
                    </h1>
                </footer>
            </>
        )
    }


}
