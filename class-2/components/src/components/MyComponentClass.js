import React from 'react'

class MyComponentClass extends React.Component {
    render() {

        const { message } = this.props

        return (
            <>
                <h1>Hi world desde componente de tipo clase</h1>
                <h2>
                    {message}
                </h2>
            </>
        )
    }
}

export default MyComponentClass