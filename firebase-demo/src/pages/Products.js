import { collection, getDocs } from "firebase/firestore"
import { db } from "../firebase"
import { useEffect } from "react"
import ProductList from "../components/products/ProductList"
import { getAllProducts } from "../actions/products"
import { useDispatch } from "react-redux"

export default function Products() {

    const dispatch = useDispatch()

    const init = async () => {
        dispatch(getAllProducts())
    }

    useEffect(() => {
        init()
    }, [])

    return (
        <>
            <ProductList />
        </>
    )
}