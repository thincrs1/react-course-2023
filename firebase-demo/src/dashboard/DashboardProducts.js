
import { PhotoIcon, UserCircleIcon } from '@heroicons/react/24/solid'
import { createNewProduct } from '../actions/products'
import { useDispatch } from 'react-redux'
import TextInput from '../uti-kit/TextInput'
import { useState } from 'react'
import TextArea from '../uti-kit/TextArea'
import Loading from '../components/Loading'
import Swal from 'sweetalert2'
import { HexColorPicker } from "react-colorful";


export default function DashboardProducts() {

    const dispatch = useDispatch()

    const [newProduct, setNewProduct] = useState({
        name: '',
        description: '',
        price: 0,
        photo: '',
        color: '#aabbcc'
    })

    const [loading, setLoading] = useState()


    const createProduct = async () => {
        setLoading(true)

        try {
            await dispatch(createNewProduct(newProduct))
            Swal.fire('El producto se agrego de manera exitosa')

        } catch (err) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'El producto no se pudo agregar de manera correcta!',
            })
        }


        setLoading(false)
    }

    return (
        <form onSubmit={(e) => {
            e.preventDefault()
            createProduct()
        }}>

            {
                loading ? <Loading />
                    :
                    <div className="space-y-12">
                        <div className="border-b border-gray-900/10 pb-12">

                            <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">

                                <div className="col-span-full">
                                    <TextArea
                                        label='Descripción del producto'
                                        name='description'
                                        onChange={val => setNewProduct({ ...newProduct, description: val })}
                                    />
                                </div>
                                <div className="col-span-full">
                                    <TextInput
                                        label='Foto del producto'
                                        name='photo'
                                        onChange={val => setNewProduct({ ...newProduct, photo: val })}
                                    />
                                </div>
                                <div className="col-span-full">
                                    <HexColorPicker
                                        onChange={(color) => setNewProduct({ ...newProduct, color })}
                                        color={newProduct.color}
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="border-b border-gray-900/10 pb-12">
                            <h2 className="text-base font-semibold leading-7 text-gray-900">Información del producto</h2>

                            <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                                <div className="sm:col-span-3">
                                    <TextInput
                                        label='Nombre del producto'
                                        name='name'
                                        onChange={val => setNewProduct({ ...newProduct, name: val })}
                                    />
                                </div>
                                <div className="sm:col-span-3">
                                    <TextInput
                                        label='Precio del producto'
                                        name='price'
                                        onChange={val => setNewProduct({ ...newProduct, price: val })}
                                    />
                                </div>

                            </div>
                        </div>

                    </div>
            }


            <div className="mt-6 flex items-center justify-end gap-x-6">
                <button
                    type="submit"
                    className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                    disabled={loading}
                >
                    Guardar
                </button>
            </div>
        </form>
    )
}
