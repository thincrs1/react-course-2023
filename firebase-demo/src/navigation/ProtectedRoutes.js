import { useAuth } from "../context/AuthContext"
import { Navigate } from "react-router-dom"
import Loading from '../components/Loading'

export default function ProtectedRoutes({ children }) {
    const { user, loading } = useAuth()

    if (loading) return <Loading />

    if (!user) return <Navigate to='/login' />

    return <>{children}</>
}