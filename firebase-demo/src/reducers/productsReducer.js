import { ADD_SHOPPING_CART, SET_PRODUCTS, SET_PRODUCT_DETAIL } from "../types/products"

const initialState = {

    productDetail: {},
    productList: [],
    shoppingCart: []

}

function productsReducer(state = initialState, action) {

    switch (action.type) {

        case SET_PRODUCTS:
            return { ...state, productList: action.payload }

        case SET_PRODUCT_DETAIL:
            return { ...state, productDetail: action.payload }

        case ADD_SHOPPING_CART:
            if (!state.shoppingCart) {
                return { ...state, shoppingCart: [action.payload] }
            }
            return { ...state, shoppingCart: [...state.shoppingCart, action.payload] }

        default:
            return initialState

    }

}

export default productsReducer