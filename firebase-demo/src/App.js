import logo from "./logo.svg";
import "./index.css";
import MainStack from "./navigation/MainStack";

function App() {
	return <MainStack className='h-full' />;
}

export default App;
