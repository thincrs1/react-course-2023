import { collection, getDocs, addDoc } from "firebase/firestore"
import { db } from "../firebase"
import { ADD_SHOPPING_CART, SET_PRODUCTS, SET_PRODUCT_DETAIL } from "../types/products"

const setProducts = products => ({
    type: SET_PRODUCTS,
    payload: products
})

export const setProductDetail = product => ({
    type: SET_PRODUCT_DETAIL,
    payload: product
})

export const addShoppingCart = product => ({
    type: ADD_SHOPPING_CART,
    payload: product
})

export const getAllProducts = () => dispatch => {
    return new Promise(async (resolve, reject) => {
        const snapshot = await getDocs(collection(db, 'products'))

        let results = []

        snapshot.forEach((doc) => {
            results.push({ id: doc.id, ...doc.data() })
        })

        dispatch(setProducts(results))
    })
}

export const createNewProduct = product => async dispatch => {
    try {
        const newProduct = await addDoc(collection(db, 'products'), product)
        return newProduct
    } catch (err) {
        console.log('error creando producto', err)
    }
}


