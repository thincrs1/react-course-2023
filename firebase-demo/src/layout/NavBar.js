import { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import { useAuth } from '../context/AuthContext'
import Standard from './Standard'
import Dashboard from './Dashboard'

export default function NavBar({ children }) {
    const location = useLocation()
    const [layoutType, setLayoutType] = useState('standard')

    useEffect(() => {
        if (location.pathname.split('/').filter(path => path === 'dashboard').length > 0) {
            setLayoutType('dashboard')
        } else {
            setLayoutType('standard')
        }
    }, [location])

    return (
        <>
            {
                layoutType === 'standard'
                    ? <Standard children={children} />
                    : <Dashboard children={children} />
            }

        </>
    )
}
