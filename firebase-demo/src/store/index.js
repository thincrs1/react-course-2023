import productsReducer from '../reducers/productsReducer'
import { applyMiddleware, createStore, combineReducers } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import thunk from 'redux-thunk'

const persistConfig = {
    key: 'root',
    storage,
}

const persistedReducer = persistReducer(persistConfig, combineReducers({ products: productsReducer }))

export const store = createStore(persistedReducer, applyMiddleware(thunk))
export const persistor = persistStore(store)
